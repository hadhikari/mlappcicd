import numpy as np
from flask import Flask, request, jsonify, render_template
import pickle

app = Flask(__name__)
model = pickle.load(open('model.pkl', 'rb'))

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/predict',methods=['POST'])
def predict():
    '''
    For rendering results on HTML GUI
    '''
    int_features = [float(x) for x in request.form.values()]
    # print(int_features)

    for i in int_features:
        if i > 10 or type(i) == str:
            return render_template('index.html', prediction_text='Error sepal lenghts cannot be more than 10 or string')

    final_features = [np.array(int_features)]
    prediction = model.predict(final_features)
    # print(final_features, prediction)
    # print(prediction[0])
    output = float((prediction[0]))


    if output ==0:
        final = 'Iris-setosa'
    elif output ==1:
        final = 'Iris-versicolor'
    else :
        final ='Iris-virginica'

    return render_template('index.html', prediction_text='Predicted Species of Flower is {}'.format(final))


# if __name__ == "__main__":
#     app.run(host = '0.0.0.0', port= 8000, debug=True)
